<?php

namespace NextOpBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('@NextOp/Default/index.html.twig');
    }
}
