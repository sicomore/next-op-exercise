<?php

/* @NextOp/XXXarticle/index.html.twig */
class __TwigTemplate_a887e3536a3622922692a284be47fb10c239b20e7e10fa2ca392daf853c29a17 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("@NextOp/Default/index.html.twig", "@NextOp/XXXarticle/index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@NextOp/Default/index.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@NextOp/XXXarticle/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@NextOp/XXXarticle/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
<section class=\"bg-light\" id=\"portfolio\">
  <div class=\"container\">
    <div class=\"row\">
      <div class=\"col-lg-12 text-center\">
        <h2 class=\"section-heading text-uppercase\">Liste des Articles</h2>
      </div>
    </div>

    <a class=\"btn btn-primary\" href=\"";
        // line 13
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("article_new");
        echo "\">Créer un nouvel article</a>

    <div class=\"row\">

    ";
        // line 17
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["articles"]) || array_key_exists("articles", $context) ? $context["articles"] : (function () { throw new Twig_Error_Runtime('Variable "articles" does not exist.', 17, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["article"]) {
            // line 18
            echo "
      <div class=\"col-md-4 col-sm-6 portfolio-item\">
        <a class=\"portfolio-link\" data-toggle=\"modal\" href=\"#portfolioModal1\">
          <img class=\"img-fluid\" src=\"img/portfolio/01-thumbnail.jpg\" alt=\"\">
        </a>
        <div class=\"portfolio-caption\">
          <h4><a href=\"";
            // line 24
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("article_show", array("id" => twig_get_attribute($this->env, $this->source, $context["article"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["article"], "title", array()), "html", null, true);
            echo "</a></h4>
          <p class=\"text-muted\">";
            // line 25
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["article"], "content", array()), "html", null, true);
            echo "</p>
          <p class=\"lead mb-0\">Création : ";
            // line 26
            if (twig_get_attribute($this->env, $this->source, $context["article"], "createdAt", array())) {
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["article"], "createdAt", array()), "Y-m-d"), "html", null, true);
            }
            echo "</p>
          <p class=\"lead mb-0\">Mise à jour : ";
            // line 27
            if (twig_get_attribute($this->env, $this->source, $context["article"], "updatedAt", array())) {
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["article"], "updatedAt", array()), "Y-m-d"), "html", null, true);
            }
            echo "</p>
          <a class=\"col-xs-6 btn btn-info\" href=\"";
            // line 28
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("article_show", array("id" => twig_get_attribute($this->env, $this->source, $context["article"], "id", array()))), "html", null, true);
            echo "\">Voir</a>
          <a class=\"col-xs-6 btn btn-warning\" href=\"";
            // line 29
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("article_edit", array("id" => twig_get_attribute($this->env, $this->source, $context["article"], "id", array()))), "html", null, true);
            echo "\">Modifier</a>
        </div>
      </div>

      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['article'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 34
        echo "
    </div>

  </div>
</section>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "@NextOp/XXXarticle/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  120 => 34,  109 => 29,  105 => 28,  99 => 27,  93 => 26,  89 => 25,  83 => 24,  75 => 18,  71 => 17,  64 => 13,  53 => 4,  44 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@NextOp/Default/index.html.twig' %}

{% block body %}

<section class=\"bg-light\" id=\"portfolio\">
  <div class=\"container\">
    <div class=\"row\">
      <div class=\"col-lg-12 text-center\">
        <h2 class=\"section-heading text-uppercase\">Liste des Articles</h2>
      </div>
    </div>

    <a class=\"btn btn-primary\" href=\"{{ path('article_new') }}\">Créer un nouvel article</a>

    <div class=\"row\">

    {% for article in articles %}

      <div class=\"col-md-4 col-sm-6 portfolio-item\">
        <a class=\"portfolio-link\" data-toggle=\"modal\" href=\"#portfolioModal1\">
          <img class=\"img-fluid\" src=\"img/portfolio/01-thumbnail.jpg\" alt=\"\">
        </a>
        <div class=\"portfolio-caption\">
          <h4><a href=\"{{ path('article_show', { 'id': article.id }) }}\">{{ article.title }}</a></h4>
          <p class=\"text-muted\">{{ article.content }}</p>
          <p class=\"lead mb-0\">Création : {% if article.createdAt %}{{ article.createdAt|date('Y-m-d') }}{% endif %}</p>
          <p class=\"lead mb-0\">Mise à jour : {% if article.updatedAt %}{{ article.updatedAt|date('Y-m-d') }}{% endif %}</p>
          <a class=\"col-xs-6 btn btn-info\" href=\"{{ path('article_show', { 'id': article.id }) }}\">Voir</a>
          <a class=\"col-xs-6 btn btn-warning\" href=\"{{ path('article_edit', { 'id': article.id }) }}\">Modifier</a>
        </div>
      </div>

      {% endfor %}

    </div>

  </div>
</section>

{% endblock %}
", "@NextOp/XXXarticle/index.html.twig", "/Users/sicomore/Documents/PRO/Etudes-Pro/Etudes-Diplomes/Google_Drive_MEB/IFOCOP/IFOCOP/Cours/Symfony/next_op_exercise/next-op-exercise/src/NextOpBundle/Resources/views/XXXarticle/index.html.twig");
    }
}
