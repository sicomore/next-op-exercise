<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($rawPathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($rawPathinfo);
        $trimmedPathinfo = rtrim($pathinfo, '/');
        $context = $this->context;
        $request = $this->request ?: $this->createRequest($pathinfo);
        $requestMethod = $canonicalMethod = $context->getMethod();

        if ('HEAD' === $requestMethod) {
            $canonicalMethod = 'GET';
        }

        // next_op_homepage
        if ('' === $trimmedPathinfo) {
            $ret = array (  '_controller' => 'NextOpBundle\\Controller\\DefaultController::indexAction',  '_route' => 'next_op_homepage',);
            if ('/' === substr($pathinfo, -1)) {
                // no-op
            } elseif ('GET' !== $canonicalMethod) {
                goto not_next_op_homepage;
            } else {
                return array_replace($ret, $this->redirect($rawPathinfo.'/', 'next_op_homepage'));
            }

            return $ret;
        }
        not_next_op_homepage:

        if (0 === strpos($pathinfo, '/article')) {
            // article_index
            if ('/article' === $trimmedPathinfo) {
                $ret = array (  '_controller' => 'NextOpBundle\\Controller\\ArticleController::indexAction',  '_route' => 'article_index',);
                if ('/' === substr($pathinfo, -1)) {
                    // no-op
                } elseif ('GET' !== $canonicalMethod) {
                    goto not_article_index;
                } else {
                    return array_replace($ret, $this->redirect($rawPathinfo.'/', 'article_index'));
                }

                if (!in_array($canonicalMethod, array('GET'))) {
                    $allow = array_merge($allow, array('GET'));
                    goto not_article_index;
                }

                return $ret;
            }
            not_article_index:

            // article_show
            if (preg_match('#^/article/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                $ret = $this->mergeDefaults(array_replace($matches, array('_route' => 'article_show')), array (  '_controller' => 'NextOpBundle\\Controller\\ArticleController::showAction',));
                if (!in_array($canonicalMethod, array('GET'))) {
                    $allow = array_merge($allow, array('GET'));
                    goto not_article_show;
                }

                return $ret;
            }
            not_article_show:

            // article_new
            if ('/article/new' === $pathinfo) {
                $ret = array (  '_controller' => 'NextOpBundle\\Controller\\ArticleController::newAction',  '_route' => 'article_new',);
                if (!in_array($canonicalMethod, array('GET', 'POST'))) {
                    $allow = array_merge($allow, array('GET', 'POST'));
                    goto not_article_new;
                }

                return $ret;
            }
            not_article_new:

            // article_edit
            if (preg_match('#^/article/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                $ret = $this->mergeDefaults(array_replace($matches, array('_route' => 'article_edit')), array (  '_controller' => 'NextOpBundle\\Controller\\ArticleController::editAction',));
                if (!in_array($canonicalMethod, array('GET', 'POST'))) {
                    $allow = array_merge($allow, array('GET', 'POST'));
                    goto not_article_edit;
                }

                return $ret;
            }
            not_article_edit:

            // article_delete
            if (preg_match('#^/article/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                $ret = $this->mergeDefaults(array_replace($matches, array('_route' => 'article_delete')), array (  '_controller' => 'NextOpBundle\\Controller\\ArticleController::deleteAction',));
                if (!in_array($requestMethod, array('DELETE'))) {
                    $allow = array_merge($allow, array('DELETE'));
                    goto not_article_delete;
                }

                return $ret;
            }
            not_article_delete:

        }

        if ('/' === $pathinfo && !$allow) {
            throw new Symfony\Component\Routing\Exception\NoConfigurationException();
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
